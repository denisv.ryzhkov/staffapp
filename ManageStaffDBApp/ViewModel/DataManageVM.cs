﻿using iTextSharp.text.pdf;
using ManageStaffDBApp.Model;
using ManageStaffDBApp.Model.Service;
using ManageStaffDBApp.View;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Odbc;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection.PortableExecutable;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace ManageStaffDBApp.ViewModel
{
    public class DataManageVM : INotifyPropertyChanged
    {
        IFileService? fileService;
        IDialogService? dialogService;

        //Get all departments
        private ObservableCollection<Department> allDepartments;
        private ObservableCollection<Document> allDocuments;
        private ObservableCollection<Position> allPositions;
        private ObservableCollection<User> allUsers;

        #region COMMANDS_LIST

        private Department? selectedDepartment;
        private Position? selectedPosition;
        private User? selectedUser;
        private Document? selectedDocument;


        private RelayCommand? addDepartment;
        private RelayCommand? addPosition;
        private RelayCommand? addUser;

        private RelayCommand? editDepartment;
        private RelayCommand? editPosition;
        private RelayCommand? editUser;

        private RelayCommand? deleteDepartment;
        private RelayCommand? deletePosition;
        private RelayCommand? deleteUser;

        private RelayCommand? addNewUser;
        private RelayCommand? editNewUser;

        private RelayCommand? addNewDepartment;
        private RelayCommand? editNewDepartment;

        private RelayCommand? addNewPosition;
        private RelayCommand? editNewPosition;

        private RelayCommand? addNewDocuments;
        private RelayCommand? editNewDocuments;
        private RelayCommand? updateCommand;
        private RelayCommand? openPdfCommand;
        private RelayCommand? setSignatureURL;

        #endregion

        //Department property
        public string DepartmentName {  get; set; }
        //Position property
        private Position _position;
        public Position position
        {
            get { return _position; }
            set
            {
                _position = value;
                OnPropertyChanged();
            }
        }
        //User Property
        public User user { get; set; }

        #region ADD_NEW_DATA_COMMAND_PROPERTIES
        public RelayCommand? AddNewDepartment
        {
            get
            {
                return addNewDepartment ?? (addNewDepartment = new RelayCommand(obj =>
                {
                    Window? window = obj as Window;
                    if (DepartmentName == null || DepartmentName.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "NameBlock");
                    }
                    else
                    {
                        string? resultStr = DataWorker.CreateDepartment(DepartmentName);

                        var lastDepartment = DataWorker.GetLastAddedDepartment();
                        if (lastDepartment != null)
                        {
                            AllDepartments.Add(lastDepartment);
                            SelectedDepartment = lastDepartment;
                        }
                        
                       // dialogService.ShowMessage(resultStr);
                        window.Close();
                    }
                }));

            }
        }
        public RelayCommand? AddNewUser
        {
            get
            {
                return addNewUser ?? (addNewUser = new RelayCommand(obj =>
                {
                    Window? window = obj as Window;
                    if (user.Name == null || user.Name.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "NameBox");
                    }
                    if (user.SurName == null || user.SurName.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "SurNameBox");
                    }
                    if (user.Email== null || user.Email.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "EmailBox");
                    }
                    if (user.Phone == null || user.Phone.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "PhoneBox");
                    }
                    if (user.Position == null)
                    {
                        dialogService.ShowMessage("Please select Position");
                    }
                    else
                    {
                        string? resultStr = DataWorker.CreateUSer(user.Name, user.SurName, user.Phone, user.Email, user.Position);
                        var lastUser = DataWorker.GetLastAddedUser();
                        if (lastUser != null)
                        {
                            AllUsers.Add(lastUser);
                            SelectedUser = lastUser;
                        }

                        dialogService.ShowMessage(resultStr);
                        window.Close();
                    }
                }));

            }
        }

        public RelayCommand? EditNewUser
        {
            get
            {
                return editNewUser ?? (editNewUser = new RelayCommand(obj =>
                {
                    Window? window = obj as Window;
                    MessageBox.Show(selectedUser.Name);
                    if (selectedUser.Name == null || selectedUser.Name.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "NameBox");
                    }
                    if (selectedUser.SurName == null || selectedUser.SurName.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "SurNameBox");
                    }
                    if (selectedUser.Email == null || selectedUser.Email.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "EmailBox");
                    }
                    if (selectedUser.Phone == null || selectedUser.Phone.Replace(" ", "").Length == 0)
                    {
                        SetRedBlockControl(window, "PhoneBox");
                    }
                    if (selectedUser.Position == null)
                    {
                        dialogService.ShowMessage("Please select Position");
                    }
                    else
                    {
                        string? resultStr = DataWorker.EditUser(user, selectedUser.Name, selectedUser.SurName, selectedUser.Phone, user.Position);
                        //var lastUser = DataWorker.GetLastAddedUser();
                        //if (lastUser != null)
                        //{
                        //    AllUsers.Add(lastUser);
                        //    SelectedUser = lastUser;
                        //}

                        dialogService.ShowMessage(resultStr);
                        window.Close();
                    }
                }));

            }
        }
        #endregion
        public RelayCommand? OpenPdfCommand
        {
            
            get
            {
                
                return openPdfCommand ?? (openPdfCommand = new RelayCommand(obj =>
                {
                    var document = obj as Document;
                    var pdfFileName = document.FilePath;
                    if (pdfFileName != null)
                        Process.Start("cmd", $"/c start \"\" \"{pdfFileName}\"");

                }));

            }
        }

        public RelayCommand? SetSignatureURL
        {

            get
            {

                return setSignatureURL ?? (setSignatureURL = new RelayCommand(async obj =>
                {
                    var document = obj as Document;
                  
                    if (document != null)
                    {
                        var pdfFileName = document.Name;
                        
                        int userId = 1;
                        string url = $"https://dr.kh.ua/sign.php?user_id={userId}&doc_name={pdfFileName}";

                        // Формируем строку <a href="...">
                        string anchorTag = $"<a href=\"{url}\">Referenz für die Unterzeichnung des Dokuments</a>";
                         await SendCodeByEmailSMTPAsync("denisv.ryzhkov@gmail.com", anchorTag);

                       // Process.Start(url);
                    }
                    else
                    {
                        MessageBox.Show("Выберите документ перед выполнением команды.", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }));

            }
        }
        public RelayCommand? UpdateCommand
        {
            get
            {
                return updateCommand ?? (updateCommand = new RelayCommand(obj =>
                {
                    var document = obj as Document;
                    if (document != null)
                    {
                        string ftpHost = "ua477381.ftp.tools";
                        string ftpUsername = "ua477381_denyardv";
                        string ftpPassword = "mMargarita212523";
                        //Get image
                        string ftpUri = $"ftp://{ftpHost}/{document.Name}.png";
                        string localFilePath = document.Name + ".png";
                        //Get Bitmap
                        string ftpBitmapURI = $"ftp://{ftpHost}/{document.Name}.dat";
                        string localFilePathBitmap = document.Name + ".dat";
                        //Get Encoded Bitmap Base64
                        string ftpBitmap_EncodedURI = $"ftp://{ftpHost}/{document.Name}_encoded.dat";
                        string localFilePath_EncodedBitmap = document.Name + "_encoded.dat";

                        try
                        {
                            using (WebClient client = new WebClient())
                            {
                                client.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                                client.DownloadFile(ftpUri, localFilePath);
                                client.DownloadFile(ftpBitmapURI, localFilePathBitmap); 
                                client.DownloadFile(ftpBitmap_EncodedURI, localFilePath_EncodedBitmap);

                                MessageBox.Show("Unterschrift des Kunden erhalten\n" + localFilePath);


                                //string bitmapSource = localFilePathBitmap;
                                //Read bit map
                                string bitmapSourceContent = File.ReadAllText(localFilePathBitmap);
                                string encodedData = File.ReadAllText(localFilePath_EncodedBitmap);

                                //string pixelArray = File.ReadAllText(localFilePath_EncodedBitmap);

                                //// Декодирование base64-строки
                                //byte[] pixelArrayBytes = Convert.FromBase64String(pixelArray);
                                //string decodedPixelArray = System.Text.Encoding.UTF8.GetString(pixelArrayBytes);

                                //// Десериализация JSON в массив пикселей
                                //Pixel[,] pixelArray2D = JsonConvert.DeserializeObject<Pixel[,]>(decodedPixelArray);

                                //// Создание изображения на основе массива пикселей
                                //Bitmap bitmap = CreateBitmapFromPixelArray(pixelArray2D);

                                //// Сохранение изображения в формате PNG
                                //bitmap.Save($"" + document.Name + "_from_Encoded_BitMap.png", System.Drawing.Imaging.ImageFormat.Png);

                                // Convert all data to the byte array

                                byte[] bitmapBytes = Array.ConvertAll(bitmapSourceContent.Split(','), byte.Parse);
                                byte[] decodedBytes = Convert.FromBase64String(encodedData);

                                //// Creating signature
                                CreateImageFromBytes(bitmapBytes, $"" + document.Name + "_fromBitMap.png", 500, 300);
                                CreateImageFromBytes(decodedBytes, $"" + document.Name + "_from_Encoded_BitMap.png", 500, 300);



                                var imagePath = localFilePath;
                                var srcPath = document.FilePath;
                                var destPath = @"" + document.Name;

                                using (var destStream = File.OpenWrite(destPath))
                                using (var reader = new PdfReader(srcPath))
                                using (var stamper = new PdfStamper(reader, destStream))
                                {

                                    var pageSize = reader.GetPageSize(1); //Get Page first page size

                                    var pdfConvertByte = stamper.GetOverContent(1);

                                    var image = iTextSharp.text.Image.GetInstance(imagePath);
                                    float scaleFactor = 50f; // Уменьшение на 50%
                                    image.ScalePercent(scaleFactor);

                                    // Устанавливаем отступы от нижнего края и правого края
                                    float marginBottom = 5;
                                    float marginRight = 5;

                                    // Установим поля PDF в ноль
                                    //stamper.FormFlattening = true;


                                    // Размещаем изображение с учетом отступов
                                    float x = pageSize.Width - image.ScaledWidth - marginRight;
                                    float y = marginBottom;

                                    image.SetAbsolutePosition(x, y);
                                    pdfConvertByte.AddImage(image);
                                    MessageBox.Show("Das Dokument wurde unterschrieben\n" + localFilePath);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"Fehler beim Herunterladen einer Datei vom FTP: {ex.Message}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Wählen Sie ein Dokument aus, bevor Sie den Befehl ausführen.", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                })
                {

                });
            }
        }

        static Bitmap CreateBitmapFromPixelArray(Pixel[,] pixelArray)
        {
            int width = pixelArray.GetLength(1);
            int height = pixelArray.GetLength(0);

            Bitmap bitmap = new Bitmap(width, height);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Pixel pixel = pixelArray[y, x];
                    Color color = Color.FromArgb(pixel.Alpha, pixel.Red, pixel.Green, pixel.Blue);
                    bitmap.SetPixel(x, y, color);
                }
            }

            return bitmap;
        }

        // Класс для представления пикселя
        class Pixel
        {
            public int Red { get; set; }
            public int Green { get; set; }
            public int Blue { get; set; }
            public int Alpha { get; set; }
        }

        static void CreateImageFromBytes(byte[] bitmapBytes, string outputPath)
        {
            int length = bitmapBytes.Length;
            //Set Signature Canvas parametrs
            int width = 600;
            int height =  400;

            //Determining the approximate dimensions of the image (square image)
            //int sideLength = (int)Math.Sqrt(length);
            //int width = sideLength;
            //int height = sideLength;

            // Creating Bitmap
            Bitmap bitmap = new Bitmap(width, height);

            // Filling Bitmap from bytes array
            for (int i = 0; i < length; i++)
            {
                int x = i % width;
                int y = i / width;
                bitmap.SetPixel(x, y, Color.FromArgb(bitmapBytes[i], bitmapBytes[i], bitmapBytes[i]));
            }

            //saving Signature
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            ImageCodecInfo encoderInfo = GetEncoderInfo("image/png");

            bitmap.Save(outputPath, encoderInfo, encoderParameters);
        }
        static void CreateImageFromBytes(byte[] bitmapBytes, string outputPath, int canvasWidth, int canvasHeight)
        {
            int length = bitmapBytes.Length;

            // Определение минимальных размеров прямоугольного изображения
            int minWidth = 1;
            int minHeight = 1;

            // Вычисление ширины и высоты с учетом минимальных значений
            int width = Math.Max(minWidth, (int)Math.Sqrt(length * canvasWidth / canvasHeight));
            int height = Math.Max(minHeight, (int)(length / width) + 1);

            // Создание Bitmap
            Bitmap bitmap = new Bitmap(width, height);

            // Заполнение Bitmap данными из массива байтов
            for (int i = 0; i < length; i++)
            {
                int x = i % width;
                int y = i / width;

                if (x < bitmap.Width && y < bitmap.Height) // Убеждаемся, что индексы в пределах размеров изображения
                {
                    bitmap.SetPixel(x, y, Color.FromArgb(bitmapBytes[i], bitmapBytes[i], bitmapBytes[i]));
                }
            }

            //saving Signature
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            ImageCodecInfo encoderInfo = GetEncoderInfo("image/png");

            bitmap.Save(outputPath, encoderInfo, encoderParameters);
        }
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo encoder in encoders)
            {
                if (encoder.MimeType == mimeType)
                {
                    return encoder;
                }
            }
            return null;
        }
        public RelayCommand? AddNewDocuments
        {
            get
            {
                return addNewDocuments ?? (addNewDocuments = new RelayCommand(obj =>
                {

                    if(dialogService.openFileDialog())
                    {
                        var filePath = dialogService.FilePath;
                        if(filePath != null)
                        {
                            var fileName = Path.GetFileName(filePath);
                            string? resultStr = DataWorker.CreateDocument(fileName, filePath);
                            var lastDocument = DataWorker.GetLastAddedDocument();
                            if (lastDocument != null)
                            {
                                AllDocuments.Add(lastDocument);
                                SelectedDocument = lastDocument;
                            }

                            dialogService.ShowMessage(resultStr);
                        }
                    }

                }));

            }
        }
        public RelayCommand? AddNewPosition
        {
            get
            {
                return addNewPosition ?? (addNewPosition = new RelayCommand(obj =>
                {
                    Window? window = obj as Window;

                        if (position.Name == null || position.Name.Replace(" ", "").Length == 0)
                        {
                            SetRedBlockControl(window, "NameBox");
                        }
                        if (position.Salary == 0)
                        {
                            SetRedBlockControl(window, "SalaryBox");
                        }
                        if (position.MaxNumber == 0)
                        {
                            SetRedBlockControl(window, "MaxBox");
                        }
                        if (position.Department == null)
                        {
                            dialogService.ShowMessage("Please select Department");
                        }
                        else
                        {
                            string? resultStr = DataWorker.CreatePosition(position.Name, position.Salary, position.MaxNumber, position.Department);
                            var lastPosition = DataWorker.GetLastAddedPosition();
                            if (lastPosition != null)
                            {
                                AllPositions.Add(lastPosition);
                                SelectedPosition = lastPosition;
                            }

                            dialogService.ShowMessage(resultStr);
                            window.Close();
                        }
                  
                }));

            }
        }
        

        #region OPEN_WINDOWS_COMMAND


        public RelayCommand? AddDepartment
        {
            get
            {
                return addDepartment ?? (addDepartment = new RelayCommand(obj=>
                {
                    AddNewDepartmentWindow addNewDepartmentWindow = new AddNewDepartmentWindow(this);
                    OpenWindow(addNewDepartmentWindow);
                }));
               
            }
        }
        public RelayCommand? AddUser
        {
            get
            {
                return addUser ?? (addUser = new RelayCommand(obj =>
                {
                    AddNewUserWindow addNewUserWindow = new AddNewUserWindow(this);
                    OpenWindow(addNewUserWindow);

                }));
            }
        }
        public RelayCommand? AddPosition
        {
            get
            {
                return addPosition ?? (addPosition = new RelayCommand(obj =>
                {
                    AddNewPositionWindow addNewPositionWindow = new AddNewPositionWindow(this);
                    OpenWindow(addNewPositionWindow);
                }));
            }
        }

        public RelayCommand? EditDepartment
        {
            get
            {
                return editDepartment ?? (editDepartment = new RelayCommand(obj =>
                {
                    EditDepartmentWindow editDepartmentWindow = new EditDepartmentWindow();
                    OpenWindow(editDepartmentWindow);
                }));
            }
        }

        public RelayCommand? EditUser
        {
            get
            {
                return editUser ?? (editUser = new RelayCommand(obj =>
                {
                    EditUserWindow editUserWindow = new EditUserWindow(this);
                    OpenWindow(editUserWindow);
                }));
            }
        }

        public RelayCommand? EditPosition
        {
            get
            {
                return editPosition ?? (editPosition = new RelayCommand(obj =>
                {
                    EditPositionWindow editPositionWindow = new EditPositionWindow();
                    OpenWindow(editPositionWindow);
                }));
            }
        }
        #endregion


        public ObservableCollection<Department> AllDepartments
        {
            get => allDepartments;
            set
            {
                allDepartments = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<Position> AllPositions
        {
            get => allPositions;
            set
            {
                allPositions = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<User> AllUsers
        {
            get => allUsers;
            set
            {
                allUsers = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Document> AllDocuments
        {
            get => allDocuments;
            set
            {
                allDocuments = value;
                OnPropertyChanged();
            }
        }

        public Department SelectedDepartment
        {
            get { return selectedDepartment; }
            set
            {
                selectedDepartment = value;
                OnPropertyChanged();
            }
        }

        public Position SelectedPosition
        {
            get { return selectedPosition; }
            set
            {
                selectedPosition = value;
                OnPropertyChanged();
            }
        }

        public User SelectedUser
        {
            get { return selectedUser; }
            set
            {
                selectedUser = value;
                
                OnPropertyChanged();
            }
        }

        private bool _isExpanded;

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (_isExpanded != value)
                {
                    _isExpanded = value;
                    OnPropertyChanged();
                }
            }
        }

        public Document SelectedDocument
        {
            get { return selectedDocument; }
            set
            {
                selectedDocument = value;
                OnPropertyChanged();
            }
        }

        // Очистка полей объекта Position
        public void ClearPosition()
        {
            position = new Position();
        }
        // Очистка полей объекта User
        public void ClearUser()
        {
            user = new User();
        }
    

        public event PropertyChangedEventHandler? PropertyChanged;

        public DataManageVM(IDialogService dialogService, IFileService fileService)
        {
            this.dialogService = dialogService;
            this.fileService = fileService;
            AllDepartments = new ObservableCollection<Department>(DataWorker.GetAllDepartments());
            AllPositions = new ObservableCollection<Position>(DataWorker.GetAllPositions());
            AllUsers = new ObservableCollection<User>(DataWorker.GetAllUsers());
            AllDocuments = new ObservableCollection<Document>(DataWorker.GetAllDocuments());
            position = new Position();
            user = new User();
        }

        public async Task<bool> SendCodeByEmailSMTPAsync(string email, string code)
        {
            try
            {
               // MessageBox.Show("Начало отправки письма.");
                // Создаем объект для отправки письма
                using (MailMessage mail = new MailMessage())
                {
                    // Указываем SMTP-сервер и порт
                    SmtpClient smtpClient = new SmtpClient("mail.adm.tools", 25);
                    smtpClient.UseDefaultCredentials = false;

                    // Указываем учетные данные для аутентификации на SMTP-сервере
                    smtpClient.Credentials = new NetworkCredential("denis@dr.kh.ua", "mMargarita212523");

                    // Включаем SSL-шифрование
                    smtpClient.EnableSsl = false;

                    // Устанавливаем отправителя
                    mail.From = new MailAddress("denis@dr.kh.ua", "Denis");

                    // Устанавливаем получателя
                    mail.To.Add(email);

                    // Устанавливаем тему письма
                    mail.Subject = "=?UTF-8?B?" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("Код пiдтверження Email")) + "?=";

                    // Устанавливаем тело письма в формате HTML
                    mail.IsBodyHtml = true;
                    mail.Body = $@"
                <html lang=""en"">
                    <body style=""background-color: #f0f0f0; text-align: center;"">
                        <div style=""background-color: #ffff00; width: 500px; margin: 20px auto; padding: 20px;"">
                            <p style=""color: #000; font-size: 18px;"">Klicken Sie auf den Link, um das Dokument zu unterzeichnen:</p>
                            <p style=""color: #000; font-size: 24px; font-weight: bold;"">{code}</p>
                        </div>
                    </body>
                </html>
            ";

                    // Асинхронно отправляем письмо
                    await smtpClient.SendMailAsync(mail);
                }
               // MessageBox.Show("Письмо успешно отправлено.");
                return true;
            }
            catch (Exception ex)
            {
                // Обработка ошибок отправки
                MessageBox.Show($"Ошибка при отправке письма: {ex.Message}");
                return false;
            }
        }

        #region UPDATE_VIEWS
        //private void UpdateAllUsers()
        //{
        //    AllUsers = new ObservableCollection<User>(DataWorker.GetAllUsers());
        //    MainWindow.AllDepartmentsView.Items.Refresh();

        //}
        //private void UpdateAllDepartments()
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        var newDepartments = DataWorker.GetAllDepartments();
        //        AllDepartments.Clear();

        //        foreach (var department in newDepartments)
        //        {
        //            AllDepartments.Add(department);
        //        }
        //    });
        //}


        //private void UpdateAllPositions()
        //{
        //    AllPositions = new ObservableCollection<Position>(DataWorker.GetAllPositions());
        //    MainWindow.AllDepartmentsView.Items.Refresh();
        //}
        #endregion

        private void SetRedBlockControl(Window window, string blockName)
        {
            Control? bloc = window.FindName(blockName) as Control;
            if (bloc != null)
            {
                bloc.BorderBrush = System.Windows.Media.Brushes.Orange;
            }
        }
        private bool OpenWindow(Window window)
        {
            window.Owner = Application.Current.MainWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            return (bool)window.ShowDialog();
           
        }

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
