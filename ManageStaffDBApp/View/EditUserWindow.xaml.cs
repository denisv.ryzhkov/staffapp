﻿using ManageStaffDBApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ManageStaffDBApp.View
{
    /// <summary>
    /// Логика взаимодействия для EditUserWindow.xaml
    /// </summary>
    public partial class EditUserWindow : Window
    {
        private DataManageVM dataManageVM;
        public EditUserWindow(DataManageVM dataManageVM)
        {
            InitializeComponent();
            this.dataManageVM = dataManageVM;
            DataContext = this.dataManageVM;
        }
    }
}
