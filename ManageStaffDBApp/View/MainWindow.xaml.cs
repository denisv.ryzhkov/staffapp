﻿using ManageStaffDBApp.Model;
using ManageStaffDBApp.Model.Service;
using ManageStaffDBApp.ViewModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace ManageStaffDBApp.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new DataManageVM(new DefaultDialogService(), new JsonFileService());

        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Получите ListView, в котором находится ListViewItem
            ListView listView = ItemsControl.ItemsControlFromItemContainer(sender as ListViewItem) as ListView;

            // Получите данные о пользователе из выделенного элемента
            if (listView?.SelectedItem is User selectedUser)
            {
                // Установите флаг раскрытия Expander
                
                
               // selectedUser.IsExpanded = !selectedUser.IsExpanded;
            }

            // Предотвратите распространение события, чтобы не срабатывало выделение ListView
            e.Handled = true;
        }
    }
}