﻿using ManageStaffDBApp.Model.Service;
using ManageStaffDBApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ManageStaffDBApp.View
{
    /// <summary>
    /// Логика взаимодействия для AddNewDepartmentWindow.xaml
    /// </summary>
    public partial class AddNewDepartmentWindow : Window
    {
        private readonly DataManageVM mainViewModel;
        public AddNewDepartmentWindow(DataManageVM mainViewModel)
        {
            InitializeComponent();
            this.mainViewModel = mainViewModel;
            DataContext = this.mainViewModel;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
