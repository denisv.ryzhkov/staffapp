﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace ManageStaffDBApp.Model.Service
{
    public class JsonFileService : IFileService
    {
        public List<object> Open(string fileName)
        {
            List<object>? result = new List<object>();
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(List<object>));
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                result = jsonSerializer.ReadObject(fs) as List<object>;
            }
            return result;
        }

        public void Save(string fileName, List<object> phonesList)
        {
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(List<object>));
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                jsonSerializer.WriteObject(fs, phonesList);
            }
        }
    }
}
