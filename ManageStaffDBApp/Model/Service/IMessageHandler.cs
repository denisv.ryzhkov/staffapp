﻿
namespace ManageStaffDBApp.Model.Service
{
    public interface IMessageHandler
    {
        event MessageDelegate MessageReceived;
        void ShowMessage(string message);
    }
}
