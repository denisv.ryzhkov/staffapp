﻿using ManageStaffDBApp.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ManageStaffDBApp.Model.Service
{
    public delegate void MessageDelegate(string message);
    public class MessageService : IMessageHandler
    {
        public static event MessageDelegate? StaticMessageReceived;

        public event MessageDelegate? MessageReceived;

        public static void SendMessageStatic(string message)
        {
            OnStaticMessageReceived(message);
        }

        public void ShowMessage(string message)
        {
            MessageView messageView = new MessageView(message);
            messageView.Owner = Application.Current.MainWindow;
            messageView.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            messageView.ShowDialog();

            OnMessageReceived(message);
        }

        private static void OnStaticMessageReceived(string message)
        {
            StaticMessageReceived?.Invoke(message);
        }

        protected virtual void OnMessageReceived(string message)
        {
            MessageReceived?.Invoke(message);
        }
    }
}

