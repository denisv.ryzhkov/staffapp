﻿using ManageStaffDBApp.View;
using Microsoft.Win32;
using System.Windows;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace ManageStaffDBApp.Model.Service
{
        public class DefaultDialogService : IDialogService
        {
            public string FilePath { get; set; }

            public bool openFileDialog()
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "PDF Files|*.pdf";
                if (openFileDialog.ShowDialog() == true)
                    {
                        FilePath = openFileDialog.FileName;
                        return true;
                    }
                    return false;
            }

            public bool saveFileDialog()
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() == true)
                {
                    FilePath = saveFileDialog.FileName;
                    return true;
                }
                return false;
            }

            public void ShowMessage(string message)
            {
                MessageView messageView = new MessageView(message);
                messageView.Owner = Application.Current.MainWindow;
                messageView.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                messageView.ShowDialog();
            }
        }
    }

