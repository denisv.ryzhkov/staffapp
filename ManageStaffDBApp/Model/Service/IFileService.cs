﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageStaffDBApp.Model.Service
{
    public interface IFileService
    {
        List<object> Open(string fileName);
        void Save(string fileName, List<object> _list);
    }
}
