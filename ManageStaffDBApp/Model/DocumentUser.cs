﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace ManageStaffDBApp.Model
{
    public class DocumentUser
    {
        public int UserId { get; set; }
        public User? User { get; set; }

        public int DocumentId { get; set; }
        public Document? Document { get; set; }

        [Key]
        public int Id { get; set; }
    }
}
