﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ManageStaffDBApp.Model
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Phone { get; set; }
        public string? SurName { get; set; }
        public string Email { get; set; }

        public int PositionId { get; set; }
        public virtual Position? Position {  get; set; }
        public List<DocumentUser>? DocumentUsers { get; set; }
        
    }
}
