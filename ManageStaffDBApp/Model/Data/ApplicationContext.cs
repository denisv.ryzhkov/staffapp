﻿using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System.Reflection.Metadata;

namespace ManageStaffDBApp.Model.Data
{
    public class ApplicationContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Department> Departments { get; set; }

        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentUser> DocumentUsers { get; set; }

        public ApplicationContext()
        {
           Database.EnsureCreated();

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);
            //optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ManageStaffDBAppDB;Trusted_Connection=True");
            //optionsBuilder.UseSqlite("Data Source=Staff.db");
            optionsBuilder.UseMySql("server=ua477381.mysql.tools;user=ua477381_staff;password=5(ncL54_iX;database=ua477381_staff",
        Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.26-mysql"));


        }
    }
}
