﻿using ManageStaffDBApp.Model.Data;
using ManageStaffDBApp.Model.Service;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ManageStaffDBApp.Model
{
    public static class DataWorker
    {
        //Get all Departments
        public static List<Department> GetAllDepartments() 
        { 
            using(ApplicationContext context = new ApplicationContext())
            {
                var result = context.Departments.ToList();
                return result;
            }
            
        }
        public static Department? GetLastAddedDepartment()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                var lastDepartment = context.Departments.OrderByDescending(d => d.Id).FirstOrDefault();
                return lastDepartment ?? null;
            }
        }
        //Get all Positions
        public static List<Position> GetAllPositions()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                var result = context.Positions.
                    Include(p => p.Department).
                    ToList();
                return result;
            }

        }
        public static Position? GetLastAddedPosition()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                var lastPosition = context.Positions.OrderByDescending(d => d.Id).FirstOrDefault();
                return lastPosition ?? null;
            }
        }
        //Get all users
        public static List<User> GetAllUsers()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                var result = context.Users.
                    Include(u => u.Position). 
                    Include(d => d.DocumentUsers).
                    ThenInclude(du => du.Document)
                    .ToList();
               
                return result;
            }

        }
        public static User? GetLastAddedUser()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                var lastUser = context.Users.OrderByDescending(d => d.Id).FirstOrDefault();
                return lastUser ?? null;
            }
        }
        //add Department
        public static string CreateDepartment(string name)
        {
            string result = "Already exist";
          

            using(ApplicationContext context = new ApplicationContext())
            {
                bool checkIsExist = context.Departments.Any(x => x.Name == name);
                if (!checkIsExist)
                {
                    Department department = new Department { Name = name };
                    context.Departments.Add(department);
                    context.SaveChanges();
                    result = "Succes";
                }
            }

            return result;
        }


        //Add Position
        public static string CreatePosition(string name, decimal salary, int maxNumber, Department department)
        {
            string result = "Already exist";

            using (ApplicationContext context = new ApplicationContext())
            {
                bool checkIsExist = context.Positions.Any(x => x.Name == name && x.Salary == salary);
                if (!checkIsExist)
                {
                    Position position = new Position { 
                        Name = name, 
                        Salary = salary, 
                        MaxNumber = maxNumber,
                        DepartmentId = department.Id };
                    context.Positions.Add(position);
                    context.SaveChanges();
                    result = "Succes";
                }
            }

            return result;
        }
        public static string CreatePosition(Position position)
        {
            string result = "Already exist";

            using (ApplicationContext context = new ApplicationContext())
            {
                bool checkIsExist = context.Positions.Any(x => x.Name == position.Name && x.Salary == position.Salary);
                if (!checkIsExist)
                {
                    context.Positions.Add(position);
                    context.SaveChanges();
                    result = "Success";
                }
            }

            return result;
        }


        //Add user
        public static string CreateUSer(string name, string surName, string phone, string email, Position position)
        {
            string result = "Already exist";

            using (ApplicationContext context = new ApplicationContext())
            {
                bool checkIsExist = context.Users.Any(x => x.Name == name && x.Position == position);
                if (!checkIsExist)
                {
                    User user = new User
                    {
                        Name = name,
                        SurName = surName,
                        Phone = phone,
                        Email = email,
                        PositionId = position.Id
                    };
                    context.Users.Add(user);
                    context.SaveChanges();
                    result = "Succes";
                }
            }

            return result;
        }
        public static string CreateUser(User user)
        {
            string result = "Already exist";

            using (ApplicationContext context = new ApplicationContext())
            {
                bool checkIsExist = context.Users.Any(x => x.Name == user.Name && x.PositionId == user.PositionId);
                if (!checkIsExist)
                {
                    context.Users.Add(user);
                    context.SaveChanges();
                    result = "Success";
                }
            }

            return result;
        }

        //Delete Department
        public static string DeleteDepartment(Department department)
        {
            string result = "Department is not exist!";
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Departments.Remove(department);
                context.SaveChanges();
                result = "Succes! Department " + department.Name + " has removed"; 
            }

            return result;
        }
        //Delete Position

        public static string DeletePosition(Position position)
        {
            string result = "Position is not exist!";
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Positions.Remove(position);
                context.SaveChanges();
                result = "Succes! Position " + position.Name + " has removed";
            }

            return result;
        }

        //Delete User

        public static string DeleteUser(User user)
        {
            string result = "User is not exist!";
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Users.Remove(user);
                context.SaveChanges();
                result = "Succes! User " + user.Name + " has removed";
            }

            return result;
        }

        //Edit Department
        public static string EditDepartment(Department oldDepartment, string newName)
        {
            string result = "Department is not exist!";
            using (ApplicationContext context = new ApplicationContext())
            {
                Department? department = context.Departments.FirstOrDefault(x => x.Id == oldDepartment.Id);
                if (department != null)
                {
                    department.Name = newName;
                    context.SaveChanges();
                    result = "Succes! Department " + department.Name + " has editet";
                }
            }

            return result;
        }
        //Edit Position
        public static string EditPosition(Position newPosition)
        {
            string result = "Position is not exist!";

            using (ApplicationContext context = new ApplicationContext())
            {
                Position? position = context.Positions.FirstOrDefault(x => x.Id == newPosition.Id);
                if (position != null)
                {
                    position.Name = newPosition.Name;
                    position.Salary = newPosition.Salary;
                    position.MaxNumber = newPosition.MaxNumber;
                    position.DepartmentId = newPosition.DepartmentId;

                    context.SaveChanges();
                    result = "Success! Position " + position.Name + " has been edited";
                }
            }

            return result;
        }
        public static string EditPosition(Position oldPosition, string newName, decimal newSalary, int newMaxNumber, Department newDepartment)
        {
            string result = "Position is not exist!";
            using (ApplicationContext context = new ApplicationContext())
            {
                Position? position = context.Positions.FirstOrDefault(x => x.Id == oldPosition.Id);
                if (position != null)
                {
                    position.Name = newName;
                    position.Salary = newSalary;
                    position.MaxNumber = newMaxNumber;
                    position.DepartmentId = newDepartment.Id;

                    context.SaveChanges();
                    result = "Succes! Position " + position.Name + " has edited";
                }
            }

            return result;
        }
        // Edit User
        public static string EditUser(User newUser)
        {
            string result = "User is not exist!";

            using (ApplicationContext context = new ApplicationContext())
            {
                User? user = context.Users.FirstOrDefault(x => x.Id == newUser.Id);
                if (user != null)
                {
                    user.Name = newUser.Name;
                    user.SurName = newUser.SurName;
                    user.Phone = newUser.Phone;
                    user.PositionId = newUser.PositionId;

                    context.SaveChanges();
                    result = "Success! User " + user.Name + " has been edited";
                }
            }

            return result;
        }
        public static string EditUser(User oldUser, string newName, string newSurName, string newPhone, Position newPosition)
        {
            string result = "User is not exist!";
            using (ApplicationContext context = new ApplicationContext())
            {
                User? user = context.Users.FirstOrDefault(x => x.Id == oldUser.Id);
                if (user != null)
                {
                    user.SurName = newSurName;
                    user.Phone = newPhone;
                    user.Name = newName;
                    user.PositionId = newPosition.Id;

                    context.SaveChanges();
                    result = "Succes! User " + user.Name + " has edited";
                }
            }

            return result;
        }

        public static List<Document> GetAllDocuments()
        {
            
            using (ApplicationContext context = new ApplicationContext())
            {
                var result = context.Documents.ToList();
                return result;
            }
        }

        public static string CreateDocument(string name, string path)
        {
            string result = "Already exist";


            using (ApplicationContext context = new ApplicationContext())
            {
                bool checkIsExist = context.Documents.Any(x => x.Name == name);
                if (!checkIsExist)
                {
                    Document document = new Document {
                        Name = name,
                        FilePath = path
                    };
                    context.Documents.Add(document);
                    context.SaveChanges();
                    result = "Succes";
                }
            }

            return result;
        }

        public static Document? GetLastAddedDocument()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                var lastDocument = context.Documents.OrderByDescending(d => d.Id).FirstOrDefault();
                return lastDocument ?? null;
            }
        }

    }
}
